'use strict';

var _config = require('./config.json');

var program = require('commander');
var fetch = require('node-fetch');


var sourceCases = function sourceCases(chan) {
  var url = "https://newsapi.org/v2/top-headlines?sources=" + chan + "&apiKey=106276028d7e4dd2a7fc98a3bbea3b71";
  return url;
};

var sourceCases2 = function sourceCases2(chan, query1) {
  var url = "https://newsapi.org/v2/top-headlines?sources=" + chan + "&q=" + query1 + "&apiKey=106276028d7e4dd2a7fc98a3bbea3b71";
  return url;
};

var fetching = function fetching(url, psMax, flag1) {
  var query1 = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "-";

  fetch(url).then(function (res) {
    return res.json();
  }).then(function (json) {
    if (json.status == "error") {
      console.log("No results. Check your input");
    } else if (json.totalResults == 0) {
      console.log("Possibly right input but zero results from this source about", query1);
    } else {
      if (query1 != "-") {
        console.log("Articles for", query1);
      }
      var megisto = json.totalResults;
      if (flag1 == 1) {
        if (psMax < megisto) {
          megisto = psMax;
        }
      }
      for (var i = 0; i < megisto; i++) {
        console.log("Title: " + json.articles[i].title);
        console.log("Author: " + json.articles[i].author);
        console.log("Source: " + json.articles[i].source.name);
      }
    }
  });
};

program.version('0.1.0').option("--top-headlines --source [chanel]", "Add the chanel").option("--max [num]", "Add the max num").parse(process.argv);

if (program.source) {
  console.log("Top headlines for " + program.source);
  if (program.max) {
    var posMax = program.max;
    var flag1 = 1;
    var url1 = sourceCases(program.source);
    var temp = 0;
    fetching(url1, posMax, flag1);
  } else {
    var _flag = 0;
    var _posMax = 1; //tuxaios arithmos apla gia na eexw orisma
    var _url = sourceCases(program.source);
    var _temp = 0;
    fetching(_url, _posMax, _flag);
  }
} else {
  _config.keywords.forEach(function (value) {
    var flag1 = 1;
    var url2 = sourceCases2(value.source, value.query);
    var posMax = value.maxArticles;
    fetching(url2, posMax, flag1, value.query);
  });
}