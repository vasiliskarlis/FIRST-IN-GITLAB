
let program = require('commander');
let fetch = require('node-fetch');
import {keywords} from './config.json'

let sourceCases = (chan) =>{
    let url="https://newsapi.org/v2/top-headlines?sources=" + chan + "&apiKey=106276028d7e4dd2a7fc98a3bbea3b71"
    return url;
};


let sourceCases2 = (chan,query1) =>{
    let url = "https://newsapi.org/v2/top-headlines?sources=" + chan + "&q=" + query1 + "&apiKey=106276028d7e4dd2a7fc98a3bbea3b71"
    return url;
}


let fetching = (url, psMax, flag1, query1 = "-") =>{
  fetch (url)
      .then((res) => {
          return res.json();
      })
      .then((json) => {
        if (json.status == "error"){
          console.log ("No results. Check your input")
        }
        else if (json.totalResults == 0){
          console.log( "Possibly right input but zero results from this source about" ,query1)

        }
        else {
          if (query1 != "-"){
            console.log ("Articles for", query1)
          }
          let megisto = json.totalResults
          if (flag1 == 1){
              if(psMax < megisto){
                  megisto = psMax;
              }
          }
          for (var i = 0; i < megisto; i++) {
              console.log("Title: " + json.articles[i].title);
              console.log("Author: " + json.articles[i].author);
              console.log("Source: " + json.articles[i].source.name);

          }
        }
      })

}


              program
                .version('0.1.0')
                .option("--top-headlines --source [chanel]", "Add the chanel")
                .option("--max [num]", "Add the max num")
                .parse(process.argv);




if (program.source) {
      console.log ("Top headlines for " + program.source)
      if (program.max){
        let posMax = program.max
        let flag1 = 1;
        let url1 = sourceCases(program.source);
        let temp = 0;
        fetching(url1,posMax,flag1);
      }
      else{
        let flag1 = 0;
        let posMax = 1; //tuxaios arithmos apla gia na eexw orisma
        let url1 = sourceCases(program.source);
        let temp = 0;
        fetching(url1,posMax,flag1);
      }
}
else{
        keywords.forEach(function (value) {
        let flag1 = 1 ;
        let url2 = sourceCases2(value.source , value.query);
        let posMax = value.maxArticles
        fetching(url2, posMax, flag1, value.query);
});
  }
